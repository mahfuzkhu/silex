<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));


$app->get('/home', function() use($app){
	return $app['twig'] ->render('home.php.twig');
});

$app->get('/', function(){
return "how are u?";

});
$app->run();